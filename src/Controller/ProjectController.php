<?php

namespace App\Controller;

use App\Service\FileService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Project;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/project", name="project")
 */

class ProjectController extends Controller
{
    private $serializer;
    //On injecte le serializer dans le constructeur de la classe
    //contrôleur car on devra s'en servir dans toutes les méthodes
    //de celle ci.
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Project::class);

        //On utilise le serializer de symfony pour transformer une
        //entité (ou array d'entités) php au format json
        $json = $this->serializer->serialize($repo->findAll(), "json");

        //On utilise la méthode static fromJsonString de la classe
        //JsonResponse pour créer une réponse HTTP en json à partir
        //de données déjà au format JSON
        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $request, FileService $fileService) {
        //On récupère le fichier dans la request
        $image = $request->files->get("image");
        //On récupère l'url absolue de notre application
        $absoluteUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        //On utilise le fileService pour uploader l'image
        $imageUrl = $fileService->upload($image, $absoluteUrl);
               
        $project = new Project();
        $project->setTitle($request->get('project-title'));//en rouge c le name coté html
        $project->setDescription($request->get('project-description'));
        
        $project->setImage($imageUrl);

        $project->setLink($request->get('project-link'));
        $project->setGitLabLink($request->get('project-gitLabLink'));
        //On fait persister le project via doctrine
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($project);
        $manager->flush();
        //On envoie une réponse pour dire que c'est ok et dire l'id qui vient d'être ajouté
        return new JsonResponse(["id" => $project->getId()]);
    }
    
    /**
     * @Route("/{project}", methods={"GET"})
     */
    public function single(Project $project) {
        $json = $this->serializer->serialize($project,"json");
        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/{project}", methods={"DELETE"})
     */
    public function remove(Project $project) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($project);
        $manager->flush();
        return new Response("", 204);
    }
    
    /**
     * @Route("/{project}", methods={"PUT"})
     */
    public function update(Project $project, Request $request) {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Project::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $project->setTitle($updated->getTitle());
        $project->setDescription($updated->getDescription());
        $project->setImage($updated->getImage());
        $project->setLink($updated->getLink());
        $project->setGitLabLink($updated->getGitLabLink());
        
        $manager->flush();
        return new Response("", 204);
    }

}